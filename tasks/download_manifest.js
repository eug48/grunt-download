/*
 * grunt-download
 * https://github.com/eug48/grunt-download
 *
 * Copyright (c) 2013 
 * Licensed under the MIT license.
 */

async = require('async');
url = require('url');
http = require('http');
fs = require('fs');
path = require('path');
mkdirp = require('mkdirp');

function clone(obj){ // from http://stackoverflow.com/a/122190/94078
      if(obj == null || typeof(obj) != 'object')
        return obj;

      var temp = obj.constructor(); // changed

      for(var key in obj)
        temp[key] = clone(obj[key]);
      return temp;
}

module.exports = function(grunt) {

  // Please see the grunt documentation for more information regarding task and
  // helper creation: https://github.com/gruntjs/grunt/blob/master/docs/toc.md

  // ==========================================================================
  // TASKS
  // ==========================================================================

  grunt.registerMultiTask('download', 'Downloads files via HTTP', function() {
    var configData = this.data;
    var done = this.async();

    var downloadFile = function(parsedUrl, outputFilename, cb) {
      var options = {
        hostname: parsedUrl.hostname,
        port: parsedUrl.port,
        path: parsedUrl.path,
        method: 'GET'
      };

      grunt.log.write('downloading ' + url.format(parsedUrl) + ' to ' + outputFilename);
      var file = fs.createWriteStream(outputFilename);

      var req = http.request(options, function(res) {
        grunt.log.write(' --> ' + res.statusCode + '\n');
        //console.log('HEADERS: ' + JSON.stringify(res.headers));
        //res.setEncoding('utf8');
        if (res.statusCode == 200) {
          res.pipe(file);
          res.on('end', function () {
            cb();
          });
        } else {
          cb('HTTP result was ' + res.statusCode);
        }
      });
      req.on('error', function(e) {
          cb('problem with request: ' +  e.message);
      });
      req.end();
    };

    var parsedUrl = url.parse(configData.url, true);
    var filename = configData.filename;
    if (fs.statSync(configData.filename).isDirectory()) {
      filename = path.join(configData.filename, path.basename(parsedUrl.pathname));
    }

    downloadFile(parsedUrl, filename, function(error) {
      if (error) {
        grunt.log.error(error);
        console.log('error: ' + error);
        done(false);
        return;
      }

      if (configData.manifest) {
        var manifest = fs.readFileSync(filename, 'utf8');
        var manifestDir = path.dirname(filename);

        var tasks = [];
        manifest.split('\n').forEach(function (line, i) {
          line = line.replace(/^\s+/g, ' ');
          if (/^CACHE MANIFEST|#|\*$/.test(line) || /^[A-Z]+:$/.test(line) || line.length == 0) {
            return;
          }

          var newUrl = clone(parsedUrl);
          newUrl.search = '';
          if (line[0] === '/') {
            newUrl.path = newUrl.pathname = line;
          } else {
            newUrl.path = newUrl.pathname = path.join(path.dirname(parsedUrl.pathname), line);
          }

          var assetFilename = path.join(manifestDir, line);
          mkdirp.sync(path.dirname(assetFilename));

          tasks.push(function(cb) { downloadFile(newUrl, assetFilename, cb); });
        });


        async.series(tasks, function(error, results) {
          if (error) {
            done(false);
          } else {
            done(true);
          }
        });
      } else {
        done(true);
      }

    });

  });

};

/* vim: set et sw=2 ts=2: */
