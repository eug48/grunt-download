# grunt-download

Downloads files via HTTP. Can download all the resources listed in an HTML5 appcache manifest. My use-case is generating a static distribution of my webapp by downloading it from the local development server, which does minification/compilation/etc.

## Getting Started
Install this grunt plugin next to your project's [grunt.js gruntfile][getting_started] with: `npm install grunt-download`

Then add it to your project's `grunt.js` gruntfile, for example:

```javascript
grunt.loadNpmTasks('grunt-download');

...
    download: {
        webapp_release: {
          url: 'http://localhost:8081/manifest.appcache',
          manifest: true, /* the URL will be treated as an HTML5 cache manifest and all resources that it lists will be downloaded */
          filename: 'webapp/dist/'
        },
        somefile: {
          url: 'http://localhost:8081/file.json',
          manifest: false,
          filename: 'webapp/dist/'
        },
    },
...
```

[grunt]: http://gruntjs.com/
[getting_started]: https://github.com/gruntjs/grunt/blob/master/docs/getting_started.md

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [grunt][grunt].

## Release History
_(Nothing yet)_

## License
Copyright (c) 2013   
Licensed under the MIT license.