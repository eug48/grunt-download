module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    test: {
      files: ['test/**/*.js']
    },
    lint: {
      files: ['grunt.js', 'tasks/**/*.js', 'test/**/*.js']
    },
    watch: {
      files: '<config:lint.files>',
      tasks: 'default'
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true,
        es5: true
      },
      globals: {}
    },
    download: {
    	m1: {
		url: 'http://www.healthyachievements.org/4670/dist/manifest.appcache',
		manifest: true,
		filename: '/tmp/t/'
	},
    	m2: {
		url: 'http://www.healthyachievements.org/4670/dist/manifest.appcache1234',
		manifest: true,
		filename: '/tmp/t/'
	}


    }
  });

  // Load local tasks.
  grunt.loadTasks('tasks');

  // Default task.
  grunt.registerTask('default', 'download test');

};
